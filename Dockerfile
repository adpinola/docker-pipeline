# Use the official lightweight Alpine image
FROM alpine:latest

# Create a directory to work in
WORKDIR /app

# Create a simple file inside the container
RUN echo "Hello, Docker!" > greeting.txt

# Print the contents of the file when the container runs
CMD cat greeting.txt
